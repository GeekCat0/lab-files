from py532lib.i2c import *
from py532lib.frame import *
from py532lib.constants import *
import paho.mqtt.client as mqtt
import time
import gpio 
client = mqtt.Client()
client.connect("192.168.1.136", 1883, 60)

# gpio.setup(505, gpio.OUT)
# gpio.setup(490, gpio.OUT)
# gpio.setup(504, gpio.OUT)

pn532 = Pn532_i2c(i2c_channel=8)
pn532.SAMconfigure()

while(True):
    card_data = pn532.read_mifare().get_data()
    # print(type(card_data))
    id = bytearray(b'K\x01\x01\x00\x04\x08\x04d\n\xd1\xaf')
    # print("ok")
    # print(card_data)
    time.sleep(1)
    if id == card_data:
        client.publish("sensors/nfc", "1")
        print("on")
        # gpio.set(490, 1)
        # gpio.set(505, 1)
        # gpio.set(504, 1)
        time.sleep(5)
        # gpio.set(490, 0)
        # gpio.set(505, 0)
        # gpio.set(504, 0)
        client.publish("sensors/nfc", "0")
        print("off")
        

