import paho.mqtt.client as mqtt
import time
client = mqtt.Client()
client.connect("localhost", 1883, 60)

while True:
    temp = "/sys/bus/iio/devices/iio:device0/in_temp_input"
    with open(temp,'r',encoding = 'utf-8') as f:
        tem = f.read()
        client.publish("sensors/bme280", "temp value="+tem)
        print(type(tem), tem)
        print("ok")
    time.sleep(2)
