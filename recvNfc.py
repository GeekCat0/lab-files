import paho.mqtt.client as mqtt
import time

def on_connect(client, userdata, flags, rc):
    client.subscribe("sensors/#")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload.decode("utf-8")))
    client.publish("led/490",msg.payload.decode("utf-8"))
    time.sleep(0.1)
    client.publish("led/505",msg.payload.decode("utf-8"))
    time.sleep(0.1)
    client.publish("led/504",msg.payload.decode("utf-8"))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("localhost", 1883, 60)

client.loop_forever()

