#!/bin/bash
MY_PATH=$(dirname $(realpath -s $0))
# Create a new tmux session named helloworld...
/usr/bin/tmux new-session -d -s helloworld
/usr/bin/tmux set-option -t helloworld remain-on-exit on
/usr/bin/tmux send-keys -t helloworld "source /root/.bashrc" C-m
/usr/bin/tmux send-keys -t helloworld "source /root/.profile" C-m
/usr/bin/tmux send-keys -t helloworld "date" C-m
/usr/bin/tmux send-keys -t helloworld "docker start mosquitto" C-m
/usr/bin/tmux send-keys -t helloworld "cd kuba_ztb/" C-m
/usr/bin/tmux send-keys -t helloworld "source virtenv/bin/activate" C-m
/usr/bin/tmux send-keys -t helloworld "python /root/kuba_ztb/button.py" C-m
/usr/bin/tmux split-window -t helloworld
/usr/bin/tmux send-keys -t helloworld "cd kuba_ztb/" C-m
/usr/bin/tmux send-keys -t helloworld "source virtenv/bin/activate" C-m
/usr/bin/tmux send-keys -t helloworld "python /root/kuba_ztb/switch.py" C-m
/usr/bin/tmux split-window -t helloworld
/usr/bin/tmux send-keys -t helloworld "cd kuba_ztb/" C-m
/usr/bin/tmux send-keys -t helloworld "source virtenv/bin/activate" C-m
/usr/bin/tmux send-keys -t helloworld "python /root/kuba_ztb/led.py" C-m

