import paho.mqtt.client as mqtt
import time

class sensor:

    def __init__(self):
        self.client = mqtt.Client()

    def on_connect(self, client, userdata, flags, rc):
            print("Connected with result code "+str(rc))
            client.subscribe("gpio/#")

    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))
        
    
        hum = "/sys/bus/iio/devices/iio:device0/in_humidityrelative_input"
        press = "/sys/bus/iio/devices/iio:device0/in_pressure_input"
        temp = "/sys/bus/iio/devices/iio:device0/in_temp_input"
        with open(temp,'r',encoding = 'utf-8') as f:
            client.publish("sensors/bme280", "temp: "+f.read())
        with open(press,'r',encoding = 'utf-8') as g:
            client.publish("sensors/bme280", "pressure: "+g.read())
        with open(hum,'r',encoding = 'utf-8') as h:
            client.publish("sensors/bme280", "humidity: "+h.read())

    def mode(self):
        
        
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.connect("localhost", 1883, 60)

        self.client.loop_forever()

tryb = sensor()
tryb.mode()
