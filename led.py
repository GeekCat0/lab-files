import paho.mqtt.client as mqtt
import gpio

gpio.setup(505, gpio.OUT)
gpio.setup(490, gpio.OUT)
gpio.setup(504, gpio.OUT)

class led:
    
    def __init__(self):
        self.client = mqtt.Client()
        

    def on_connect(self, client, userdata, flags, rc):
        client.subscribe("led/#")


    def on_message(self, client, userdata, msg):
        x = msg.topic[4:]
        y = msg.payload.decode("utf-8")
        gpio.setup(x, gpio.OUT)
        if y == "1":
            gpio.set(x, 1)
        else:
            gpio.set(x, 0)
        
    def mode(self):
    
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.connect("localhost", 1883, 60)


        self.client.loop_forever()
tryb = led()
tryb.mode()
